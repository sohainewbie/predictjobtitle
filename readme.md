- This Project use Flask Python framework 

# DEPENDENCIES

1. make sure you install pip at your pc/server
```
centos : yum install python-pip
ubuntu  : apt-get install python-pip
windows : -> https://pypi.python.org/pypi/pip , 
		  -> python setup.py 
```

2. install the library
```
pip install -r requirements.txt
```

```
if you have problem Install MYSQLmodule python
ubuntu  : sudo apt-get install python-pip python-dev libmysqlclient-dev
```

# HOW TO RUN
```
python app.py
```

#API-DOC

* HOMEPAGE
```
ENDPOINT=http://127.0.0.1:8080/
METHOD=[GET/POST]
RESPONSE=
	{
		response_code: 200,
		version: "1.0.0",
		name: "API Predicting a job title"
	}
```

* SEARCH RECOMMEND JOB TITLES
```
ENDPOINT=http://127.0.0.1:8080/recommend_job_titles?q=data+scentist&format=JSON
METHOD=[GET]
RESPONSE=
	{
		input: "data scentist",
		response_code: 200,
		result: [
			"Data Scientist",
			"Staff Scientist",
			"Chief Data Scientist"
		]
	}
```

#UNIT-TESTING
```
cd testing/
python test.py


output:
	python test.py
	TOTAL DATA= 4013
	(['Data Scientist', 'Staff Scientist', 'Scientist'], 'dat scentist')
	(['Head of Marketing Communications'], 'Executive, Promotions & Events (Marketing Communications)')
	(['Vice President, Investment Banking', 'Investment Banking Analyst'], 'Analyst, Global Investment Banking')
	.TOTAL DATA= 4013
	.
	----------------------------------------------------------------------
	Ran 2 tests in 0.148s

	OK
```

