import unittest
import os 
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+ '/app')
from models.job import *
from controllers.page import doJobSearching


class TestAllMethods(unittest.TestCase):

    def test_fetchData(self):
		#make sure data must be greater than zero
		file_location = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+ '/app/models/clean_job_titles.md'
		totalData, listData = readFile(file_location)
		print "TOTAL DATA= %d" % totalData
		self.assertGreater(totalData, 0)
		return listData

    def test_doJobSearching(self):
    	listData = self.test_fetchData() # we recall fetchData
    	sample1 = doJobSearching(listData, "dat+scentist")
    	print sample1
    	self.assertNotEqual(sample1, None)

    	sample2 = doJobSearching(listData, "Executive,+Promotions+%26+Events+(Marketing+Communications)")
    	print sample2
    	self.assertNotEqual(sample1, None)

    	sample3 = doJobSearching(listData, "Analyst,+Global+Investment+Banking")
    	print sample3
    	self.assertNotEqual(sample1, None)



if __name__ == '__main__':
    unittest.main()