import urllib
import difflib


def fetchData(dataModel):
	from app.models.job import readFile
	totalData, listData = readFile(dataModel)
	return totalData, listData

def doJobSearching(listData, keyword):
	keyword = urllib.unquote(keyword).replace('+',' ')
	resultData = []
	inputKeyword = keyword
	if len(keyword) > 0:
		resultData = difflib.get_close_matches(keyword, listData)
	return resultData, inputKeyword