import os
import json
from app import app
from flask import request

@app.route('/', methods=['GET', 'POST'])
def index():
	response = { 'response_code' : 200, 
				 'name' : 'API Predicting a job title', 
				 'version' : '1.0.0' }
	return json.dumps(response)

@app.route('/recommend_job_titles', methods=['GET'])
def searchJobTitle():
	from app.controllers.page import fetchData
	from app.controllers.page import doJobSearching

	resultData = []
	response = {'response_code' : 204,  'message' : 'Data not found' }

	keyword = request.args.get('q')
	typeRespone = request.args.get('format')

	if typeRespone == None:
		response['message'] = 'please input type format like JSON'
	else:
		file_location = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+ '/models/clean_job_titles.md'
		totalData, listData = fetchData(file_location) #read Alldata
		if keyword and typeRespone.upper() == 'JSON':
			response = {'response_code' : 200 }
			resultData, inputKeyword = doJobSearching(listData, keyword) #try to filter data by keyword
			if len(resultData) != 0:
				response['input']  = inputKeyword
				response['result'] = resultData

	return json.dumps(response)