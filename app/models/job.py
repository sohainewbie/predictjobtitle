def readFile(dataModel):
	lines = []
	with open(dataModel) as file:
		lines = file.read().splitlines()
	return len(lines), lines